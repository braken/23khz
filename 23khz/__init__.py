#__version__ = '0.1.0'
import importlib.metadata

__version__ = importlib.metadata.version('23khz')

def version():
    print(__version__)

from flask import Flask
from .app import app
