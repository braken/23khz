from flask import Flask, render_template, request
from flask_wtf import FlaskForm
from wtforms import widgets, SelectMultipleField
from flask_misaka import Misaka
from .db import get_db, Category
import importlib.metadata

version = importlib.metadata.version('23khz')

app = Flask(__name__)
app.config.from_object(__name__)

def create_app():
    app = Flask(__name__,
                static_folder='static')
    app.config['WTF_CSRF_ENABLED'] = False
    with app.app_context():
        app.db = get_db()
    Misaka(app)
    return app

app = create_app()

@app.route('/',methods=['post','get'])
def index():
    categories = app.db.categories.values()
    return render_template('index.html', categories=categories)

@app.route('/report', methods=['get'])
def report():
    args = request.args.to_dict(flat=False)
    reasons = [app.db.get(key) for key in args.get('r', [])]
    # A set of categories
    cat_keys = {reason.category.key for reason in reasons}
    # Construct a new dict categories for the page
    categories = {c: Category(key=c, form=app.db.categories[c].form) \
                  for c in cat_keys}
    # Sort the reasons by category
    for reason in reasons:
        categories[reason.category.key].reasons[reason.key] = reason
    return render_template('success.html', categories=categories.values())

@app.route('/all')
def all():
    return render_template('all.html', db=app.db)

@app.route('/help')
def help_page():
    return render_template('help.html')

@app.route('/credits')
def credits():
    return render_template('credits.html', version=version)

if __name__ == '__main__':
    app.run(debug=True)
